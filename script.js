
$(document).ready(function () {
    asma23alamReady();
});
var asma23alam = [];
var asma2Kora = [];
var allA7rof = [];
var a7rofJar = [];
var a7rofNida2 = [];
var zarefAlZaman = [];
var zarefALMakan = [];
var asma2Ishara = [];
var rmoz = [];
var hamzat = [];
var aalamatTarkim = [];
var paragraphs = [];

var abjadya = [];

function asma23alamReady() {

    var lines = $('.input-text2').val();
    var pargs = lines.split(/\.{1}\n{1,}[ ]{3,}/); // split on dot followed by new-line(once or more) followed by spaces(3 or more) 
    for (var i = 0; i < pargs.length; i++) {
        if (pargs[i].trim() == '') {      // if only white space do not consider it as paragraph
            continue;
        }

        var sentences = pargs[i].split(/\.|,|;|:/) // split on "." or "," or ";" or ":" etc...

        extract = [];

        asma23alam.push('غسان')

        for (var y = 0; y < sentences.length; y++) {


            var eachsentence = _.words(sentences[y], /[^, ]+/g);




            for (var z = 0; z < eachsentence.length; z++) {
                asma23alam.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, " "));
                asma23alam.push(eachsentence[z]);


            }



        }




    }

    var lines = $('.input-text3').val();
    var pargs = lines.split(/\.{1}\n{1,}[ ]{3,}/); // split on dot followed by new-line(once or more) followed by spaces(3 or more) 
    for (var i = 0; i < pargs.length; i++) {
        if (pargs[i].trim() == '') {      // if only white space do not consider it as paragraph
            continue;
        }

        var sentences = pargs[i].split(/\.|,|;|:/) // split on "." or "," or ";" or ":" etc...

        extract = [];



        for (var y = 0; y < sentences.length; y++) {


            var eachsentence = _.words(sentences[y], /[^, ]+/g);




            for (var z = 0; z < eachsentence.length; z++) {
                asma2Kora.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));

                asma2Kora.push(eachsentence[z]);

            }




        }




    }

    //fill a7rofJar
    a7rofJar = ["عليهما", "منهما", "عليها", "فيها", "إليهما", "منه", "فيهما", "منهم", "إليهم", "عليهم", "عنهم", "منهمن", "إليهن", "عليهن", "عنهن", "فيهن", "عنها", "فيها", "عنها", "إلبها", "منها", "فيه", "عليه", "إليه", "من", "إلى", "على", "عن", "في", "مذ", "", "منذ", "خلا", "حاشا", "مما", "عما", "فيما", "بما", "عدا", "عنهما", "عند", "به", "بها", "بهما", "بهم", "بهن", "عليك", "له", "لها", "لهما", "لهم", "لهن", "إليك", "عنده", "عندها", "عندهما", "عندهم", "عندهن", "عليكن", "منك", "منكما", "منكم", "منكن", "عليكما", "عليكم", "عنك", "عنكما", "عنكن", "إليكم", "إليكما", "إليكن", "بك", "بكما", "بكم", "بكن", "بينك", "قد", "بعدها", "بعده", "بعد", "بعدكم", "بعدك", "عندما", "بينكن", "بينكم", "بينكما", "بينهن", "بينهم", "بينهما", "بينهما", "بينها", "بينه", "بين", "بعدهما", "بعدكن", "بعدهن"];

    // fill a7rof3atef 
    var a7rof3atef = ["و", "ثم", "ف", "حتى", "لا", "أو", "أم", "لكن", "بل", "لكنه", "لكنها", "لكنهم", "لكنهما", "لكنهن"];

    //fill zarefALZaman
    zarefAlZaman = ["فجر", "حين", "ظهر", "عشية", "بكرة", "اليوم", "أمس", "سنة", "ساعة", "صباح", "مساء", "يوم"];

    // zarefALMakan
    zarefALMakan = ["فوق", "تحت", "أمام", "وراء", "حيث", "دون", "خلف", "حولكم", "حولهم", "حولهم", "خلفهن", "خلفهم", "خلفهم", "خلفهما", "تحتهن", "تحتهما", "تحتهما", "تحتها", "تحته", "فوقهن", "فوقهم", "فوقهم", "فوهما", "فوقها", "فوقها", "فوقه", "حولكن", "حولكما", "حولهما", "حوله", "حولك", "حولهن", "خلفها", "خلفه", "دونها", "دونهن", "دونهما", "دونهما", "دونه", "وراءهن", "وراءهم", "وراءهما", "وراءها", "وراءه", "حولها", "حول", "أمامهن", "أمامهم", "أمامهما", "امامها", "أمامه"];

    // adawatAlNaseb
    var adawatAlNaseb = ["أن", "لن", "إذن", "كي", "لكي", "حتى", "يأنهم", "بأنهما", "بأنهما", "بأنها", "بأنه", "لأنها", "لأنه", "لأنهن", "لأنهم", "لأنهما", "بأن"];

    //adawatAlJazem
    var AdawatAlJazem = ["لا", "لم", "لما"];

    //adawatEtsfhm
    var adawatEtsfhm = ["هل", "من", "ما", "متى", "أيان", "كيف", "كم", "أنى", "أي", "ماذا", "لماذا", "حتى"];

    //adwatAlWasal
    var adwatAlWasal = ["الذي", "اللذين", "اللذان", "الذين", "التي", "اللتان", "اللتين", "الاتي", "اللواتي", "الاتي", "اللواتي", "اللاتي", "ما", "اي", "أيهم"];

    //adawatAlSharet
    var adawatAlSharet = ["إن", "ما", "من", "أين", "متى", "أينما", "أنى", "حيثما", "أيان", "كيفما", "لو", "لولا", "إذا", "إذاما", "أي", "مهما"];

    //inWaakhwatoha
    var inWaakhwatoha = ["إن", "أن", "كأن", "ليت", "لعل", "نفس", "فكأنه", "فأنهن", "فأنهما", "فأنها", "فأنه", "فإنهن", "فإنهم", "فإنهما", "فإنه", "انما", "انما", "إنما", "كأنهن", "كأنهم", "كأنهما", "كأنها", "كأنه", "أنهن", "أنهم", "أنهما", "أنها", "أنه", "إنهن", "إنهما", "إنها", "إنه", "ليتهن", "ليتهما", "ليتهما", "ليتها", "ليتها", "ليته", "لعلهن", "لعلهما", "لعلهما", "لعلها", "لعله"];

    //adawatTa2akod
    var adawatTa2akod = ["كل", "كلها", "كلهم", "كلهن", "كليتهما", "كلامها", "نفسه", "نفسهما", "أجمعين", "جمعاء", "أجمعون", "جميع", "جميعهم", "جميعهن", "ذاته", "ذاتها", "عينه", "عينها", "عينهم", "كلامها", "كافة", "عما", "بكل", "بكلها", "بكلهم", "بكلهن", "بكلتيهما", "بكلاهما", "بنفسه", "بنفسها", "لجميع", "بجميعهم", "بجميعهن", "بذاته", "لذاتها", "بعض", "بعضه", "بعضها", "بعضهم", "بعضهن", "لعامة", "لكافة", "لكلاهم", "لكلاهما", "بعينها", "بعينه", "بذاتها", "لذاته", "لجميعهن", "لجميعهم", "بجميع", "لنفسه", "لكلاهما", "لكلتيهما", "لكلهن", "لكلهم", "لكلها", "لكل", "ذات"];

    // adawatTashbih
    var adawatTashbih = ["نظير", "كأن", "كأنه", "كأنها", "كأنهما", "كأنهن", "شبيه", "كأنهن", "كأنهما", "كأنهم", "كأنها", "كأنه"];

    //asma2Ishara
    asma2Ishara = ["هنا", "هذا", "هذه", "هاتان", "هاذان", "هاذين", "هاتين", "هؤلاء", "ذلك", "تلك", "أولئك", "هكذا", "هناك", "فأولئك", "فتلك", "فذلك", "فهؤلاء", "فهاتين", "فهاذين", "فهاذان", "فهاذان", "فهاتان", "فهذه", "فهذا", "هنالك", "هنا"];

    //a7rofNida2
    a7rofNida2 = ["أي", "يا", "هيا", "ياأيها"];

    //a7rof alNafi
    var alNafi = ["لا", "ليس", "غير", "لم", "لما", "لن", "إن", "ما", "لات", "ليست", "فلا", "فليس", "فغير", "غيره", "غيرهما", "غيرها", "غيرها", "غيرهم", "غيرهن", "فغيره", "فغيرهن", "فغيرهم", "فغيرها", "فغيرهما"];

    //a7rofAlKasam
    var a7rofAlKasam = ["بالله", "والله", "تالله"];

    //a7rofTanbih
    var a7rofTanbih = ["ألا", "أما", "ها", "يا", "كلا", "نعم", "فأما"];

    // adawatAlRabet
    var adawatAlRabet = ["كما", "ما عدا", "باستثناء", "إلا", "لأن"];

    //alDama2er
    var alDama2er = ["هو", "هي", "هما", "هن", "هما", "انت", "أنت", "أنتما", "انتما", "انتن", "أنتن", "أنتما", "انتما", "فهم", "فانتم", "فأنتن", "فأنتما", "فأنت", "فهن", "فهما", "فهي", "فهو", "هم", "انتم"];

    var a7rofAlkasam = ["بالله", "والله", "تالله"];


    var a7rofmotafarika = ["ربما", "فربما"];

    abjadya = ['أ', 'ظ', 'ث', 'ي', 'ذ', 'ز', 'ط', 'غ', 'د', 'ج', 'ض', 'ع', 'ا', 'ف', 'س', 'ص', 'خ', 'ن', 'ك', 'س', 'ت', 'ب', 'ر', 'و', 'ق', 'ح', 'ه'];

    //damej array a7rof
    allA7rof = a7rofJar.concat(a7rof3atef).concat(zarefAlZaman).concat(zarefALMakan).concat(adawatAlNaseb)
        .concat(AdawatAlJazem).concat(adawatEtsfhm).concat(adwatAlWasal).concat(adawatAlSharet).concat(inWaakhwatoha).concat(adawatTa2akod)
        .concat(adawatTashbih).concat(asma2Ishara).concat(a7rofNida2).concat(alNafi).concat(a7rofAlKasam).concat(adawatAlRabet).concat(alDama2er).concat(a7rofAlkasam).concat(a7rofmotafarika);

    hamzat = ['ُ', 'ً', 'َ', 'ِ', 'ٍ', 'ٌ'];

    //rmoz
    rmoz = ["@", "#", "$", "%", "^", "&", "*", "(", ")", "<", ">", "|", '"', "[", "]", "~", "{", "}", "/", "+", '\\', '’'].concat(hamzat);

    aalamatTarkim = [".", "،", "؛", ":", "؟", "!"]

}


function analyzeText() {

    //fill hamzat



    var target;
    if (document.getElementById('fakart').checked) {
        target = 'fakart';
    }
    else if (document.getElementById('jomal').checked) {
        target = 'jomal';

    } 
else if (document.getElementById('word').checked) {
        target = 'word';

    }
else {
        target = 'others';

    }

    // clear old values
    paragraphs = [];

    var table = $('.result-table');
    var body = table.find('tbody');
    body.empty();
    table.hide();

    var text = $('.input-text').val();






    // check if no text
    if (!text || text.trim() == '') {
        alert('ّﺺﻨﻟا ﻝﺎﺧﺩﺇ ءﺎﺟﺮﻟا');
        return;
    }

    var orginaltext = text;
    text = '&nbsp &nbsp &nbsp' + text;
    // extract paragraphs
    var pargs = text.split(/\.*?[?!.]{1}\n{1,}[ ]{3,}/); // split on dot followed by new-line(once or more) followed by spaces(3 or more) 
    if (pargs.length == 0) {
        alert('ّﺺﻨﻟا ﻝﺎﺧﺩﺇ ءﺎﺟﺮﻟا');
        return;
    }


    extract = [];




    // extract sentences
    for (var i = 0; i < pargs.length; i++) {
        if (pargs[i].trim() == '') {      // if only white space do not consider it as paragraph
            continue;
        }
        if (target == 'fakart') {
            extract[i] = {
                sentences: ' ',
		words : '',
                characters: '',
                number: '',
                Nouns: '',
                others: '',
                verbes: '',
                codes: '',
                numbering: '',
                numbering: '',

            }
        }
        else if (target == 'jomal') {
            var sentences = pargs[i].match(/[^.،.؛:؟!]+[.،.؛:؟!]+[\])]*|.+/g); // split on "." or "," or ";" or ":" etc...
            sentences = extractSenteces (sentences);

            extract = [];
            for (var y = 0; y < sentences.length; y++) {
                extract[y] = {
                    sentences: sentences[y],
		words:'',
                    characters: '',
                    number: '',
                    Nouns: '',
                    others: '',
                    verbes: '',
                    codes: '',
                    numbering: '',
                    expctionCase: '',
                }
            }


        }
 else if (target == 'word') {
            var sentences = pargs[i].match(/[^.،.؛:؟!]+[.،.؛:؟!]+[\])]*|.+/g); // split on "." or "," or ";" or ":" etc...
 
            sentences = extractSenteces (sentences);



            var characters = "";
            extract = [];





            for (var y = 0; y < sentences.length; y++) {





                var eachsentence = _.words(sentences[y].trim(), /[^, ]+/g);
var wordsArray = [];
                var a7rofArray = [];
                var ar2amArray = [];
                var asma2Array = [];
                var verbsArray = [];
                var others = [];
                var rmozArray = [];
                var expctionCaseArray = []
                var tarkimArray = [];
               



                if (eachsentence.length <= 0) {
                    continue;
                }


                eachsentence = shiftingLastWord(eachsentence);



                for (var z = 0; z < eachsentence.length; z++) {

console.log('123');
                    if ((!isNaN(eachsentence[z][0]) || abjadya.includes(eachsentence[z][0])) && eachsentence[z][1] == '-') {
                        

                    }
					
                    else if (!isNaN(eachsentence[z])) {
						
                        ar2amArray.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }
                    // check if t3dad 1.
                    else if ((!isNaN(eachsentence[z][0]) || abjadya.includes(eachsentence[z][0])) && eachsentence[z][1] == '.') {
                        
                    }
                    else if (eachsentence[z] == 'ش.' && eachsentence[z + 1] == "م." && eachsentence[z + 2] == "ل.") {

                        
                        z = z + 2;

                    }



                    else if (allA7rof.includes(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""))) {
                        a7rofArray.push(eachsentence[z]);
                    }

                    else if (rmoz.includes(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""))) {
                       

                    }
                    else if (aalamatTarkim.includes(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""))) {
                      
                    }


                    else if (eachsentence[z].length < 2) {
                        
                    }
                    else if (isLetter(eachsentence[z])) {
                        asma2Array.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }
                    else if (z > 0 && (a7rofJar.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || a7rofJar.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || a7rofNida2.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || zarefAlZaman.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || zarefALMakan.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || asma2Ishara.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")))) {
                        asma2Array.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));

                    }
                    else if (!isArabic(eachsentence[z])) {

                    }
                    else if (isSpecailCaseByPoint(eachsentence[z])) {
                       

                    }
                    else {
                        verbsArray.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }

                }

                characters ='';
if(a7rofArray.length>0)
wordsArray = a7rofArray
if(asma2Array.length>0)
wordsArray =wordsArray.concat(asma2Array);
if(verbsArray.length>0)
  wordsArray =wordsArray.concat(verbsArray);
              
                Nouns ='';
 words = wordsArray.toString(); 
                
               
                verbes = '';

                
               
                extract[y] = {
                    sentences: sentences[y],
                    characters: characters,
words:words,
                    number: '',
                    Nouns: Nouns,
                    others: '',
                    verbes: verbes,
                    codes: '',
                    numbering: '',
                    expctionCase: ''

                };


            }



        }
        else {
            var sentences = pargs[i].match(/[^.،.؛:؟!]+[.،.؛:؟!]+[\])]*|.+/g); // split on "." or "," or ";" or ":" etc...
 
            sentences = extractSenteces (sentences);



            var characters = "";
            extract = [];





            for (var y = 0; y < sentences.length; y++) {





                var eachsentence = _.words(sentences[y].trim(), /[^, ]+/g);
var wordsArray = [];
                var a7rofArray = [];
                var ar2amArray = [];
                var asma2Array = [];
                var verbsArray = [];
                var others = [];
                var rmozArray = [];
                var expctionCaseArray = []
                var tarkimArray = [];



                if (eachsentence.length <= 0) {
                    continue;
                }


                eachsentence = shiftingLastWord(eachsentence);



                for (var z = 0; z < eachsentence.length; z++) {

console.log('123');
                    if ((!isNaN(eachsentence[z][0]) || abjadya.includes(eachsentence[z][0])) && eachsentence[z][1] == '-') {
                        expctionCaseArray.push(eachsentence[z]);

                    }
					
                    else if (!isNaN(eachsentence[z])) {
						
                        ar2amArray.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }
                    // check if t3dad 1.
                    else if ((!isNaN(eachsentence[z][0]) || abjadya.includes(eachsentence[z][0])) && eachsentence[z][1] == '.') {
                        expctionCaseArray.push(eachsentence[z]);
                    }
                    else if (eachsentence[z] == 'ش.' && eachsentence[z + 1] == "م." && eachsentence[z + 2] == "ل.") {

                        expctionCaseArray.push(' ش.م.ل');
                        z = z + 2;

                    }



                    else if (allA7rof.includes(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""))) {
                        a7rofArray.push(eachsentence[z]);
                    }

                    else if (rmoz.includes(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""))) {
                        rmozArray.push(eachsentence[z]);

                    }
                    else if (aalamatTarkim.includes(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""))) {
                        tarkimArray.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }


                    else if (eachsentence[z].length < 2) {
                        others.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }
                    else if (isLetter(eachsentence[z])) {
                        asma2Array.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }
                    else if (z > 0 && (a7rofJar.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || a7rofJar.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || a7rofNida2.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || zarefAlZaman.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || zarefALMakan.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")) || asma2Ishara.includes(eachsentence[z - 1].replace(/(\r\n|\n|\r)/gm, "")))) {
                        asma2Array.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));

                    }
                    else if (!isArabic(eachsentence[z])) {

                    }
                    else if (isSpecailCaseByPoint(eachsentence[z])) {
                        expctionCaseArray.push(eachsentence[z]);

                    }
                    else {
                        verbsArray.push(eachsentence[z].replace(/(\r\n|\n|\r)/gm, ""));
                    }

                }

                characters = a7rofArray.toString();
if(a7rofArray.length>0)
wordsArray = a7rofArray
if(asma2Array.length>0)
wordsArray =wordsArray.concat(asma2Array);
if(verbsArray.length>0)
  wordsArray =wordsArray.concat(verbsArray);
              number = ar2amArray.toString();
                Nouns = asma2Array.toString();
 words = wordsArray.toString(); 
                codes = rmozArray.toString();
                otherssString = others.toString();
                verbes = verbsArray.toString();

                numbering = tarkimArray.toString();
                expctionCase = expctionCaseArray.toString();
                extract[y] = {
                    sentences: sentences[y],
                    characters: characters,
words:words,
                    number: number,
                    Nouns: Nouns,
                    others: otherssString,
                    verbes: verbes,
                    codes: codes,
                    numbering: numbering,
                    expctionCase: expctionCase,

                };


            }



        }


        var pargs = orginaltext.split(/\.*?[?!.]{1}\n{1,}[ ]{3,}/); // split on dot followed by new-line(once or more) followed by spaces(3 or more) 



        paragraphs[i] = {

            parg: pargs[i],

            extract: extract,

        };

    }











    // fill result table
    for (p of paragraphs) {
var e=1;
        for (var i = 0; i < p.extract.length; i++) { // loop over sentences of each paragraph
            var row;
 
var rowBorderBold = "";

if(e == p.extract.length)
rowBorderBold =' style="border-bottom:5pt solid black;" ';
else
rowBorderBold  ='';
            if (i == 0) {

                row = $('<tr  ' + rowBorderBold  +'>' +
                    '<td rowspan="' + p.extract.length + '" style="color:#659be0;">' + p.parg + '</td>' +
                    '<td style="color:#47c931;">' + p.extract[i].sentences + '</td>' + 
 '<td>' + p.extract[i].words + '</td>' +
                    '<td>' + p.extract[i].Nouns + '</td>' +

                   '<td>' + p.extract[i].verbes + '</td>' + // af3al
                   '<td>' + p.extract[i].characters + '</td>' + // ...
                   '<td>' + p.extract[i].number + '</td>' +
                    '<td>' + p.extract[i].codes + '</td>' +
                   '<td>' + p.extract[i].numbering + '</td>' +

                    '<td>' + p.extract[i].expctionCase + '</td>' +



                   '<td>' + p.extract[i].others + '</td>' +

                    '<td>' + '' + '</td>' +
                    '</tr>');
            } else {
			 
                row = $('<tr' + rowBorderBold + '>' +
                    // remove first <td> for row spanning
                    '<td style="color:#47c931;">' + p.extract[i].sentences + '</td>' +
 '<td>' + p.extract[i].words + '</td>' +
                   '<td>' + p.extract[i].Nouns + '</td>' +

                    '<td>' + p.extract[i].verbes + '</td>' + // af3al
                    '<td>' + p.extract[i].characters + '</td>' + // ...
                    '<td>' + p.extract[i].number + '</td>' +
                    '<td>' + p.extract[i].codes + '</td>' +
                    '<td>' + p.extract[i].numbering + '</td>' +
                    '<td>' + p.extract[i].expctionCase + '</td>' +

                    '<td>' + p.extract[i].others + '</td>' +

                    '<td>' + '' + '</td>' +
                    '</tr>');
            }
e=e+1;
            body.append(row);
        }

        setTimeout(function () {
            table.show();
        }, 300); // just for animation



    }

    document.getElementById("export-btn").style.display = 'inline-block';




}

function shiftingLastWord(eachsentence) {

    if (eachsentence.length > 0) {
        var lastword = (eachsentence[eachsentence.length - 1].replace(/(\r\n|\n|\r)/gm, ""));

        var wordToShiffting = lastword.split('.');


        if (!isNaN(wordToShiffting[1]) || isSpecailCaseByPoint(lastword)) {


        } else {
            eachsentence[eachsentence.length - 1] = wordToShiffting[0];

        }
        return eachsentence;
    }


}


function mergerSentenceIfisSpecailCaseByPoint(eachsentence) {
    if (eachsentence.length > 0) {

        var lastword = (eachsentence[eachsentence.length - 1].replace(/(\r\n|\n|\r)/gm, ""));

        return isSpecailCaseByPoint(lastword);

    }

}

function isSpecailCaseByPoint(eachsentence) {
    if (eachsentence.length == 4 && eachsentence.includes('.')) {

        var wordToShiffting = eachsentence.split('.');

        if (wordToShiffting[0].length == 1 && wordToShiffting[1].length == 1) {

            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;

    }
}





function isLetter(eachsentence) {

    var pattern1 = 'ال';
    var pattern2 = 'أل';
    var pattern3 = 'اْل';

    var pattern4 = 'ة';



    var pattern5 = 'وال';

    var pattern6 = 'كال';
    var pattern7 = 'بال';
    var pattern8 = 'فال';
    var pattern9 = 'لل';

    var pattern10 = 'ات';

    var pattern11 = 'اة';

    if (eachsentence.startsWith(pattern1) || eachsentence.startsWith(pattern2) || eachsentence.startsWith(pattern3) || eachsentence.endsWith(pattern4) || eachsentence.endsWith(pattern10) || eachsentence.endsWith(pattern11) || eachsentence.startsWith(pattern5) || eachsentence.startsWith(pattern6) || eachsentence.startsWith(pattern7) || eachsentence.startsWith(pattern8) || eachsentence.startsWith(pattern9)) {

        return true;
    }




    else if (hamzat.includes(eachsentence.charAt(1)) || hamzat.includes(eachsentence.charAt(eachsentence.length - 1))) {
        return true;

    }
    else if (asma23alam.includes(eachsentence)) {
        return true;
    }
    else if (asma2Kora.includes(eachsentence)) {
        return true;
    }


    else {
        return false;
    }

}


function isArabic(text) {
    var pattern = /[\u0600-\u06FF\u0750-\u077F]/;
    result = pattern.test(text);
    return result;
}

function exportTofile() {






    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(syntaxHighlight(paragraphs)));
    element.setAttribute('download', 'إقرأ.txt');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}


function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = '';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {

            } else {

            }
        } else if (/true|false/.extract(match)) {
            cls = 'boolean';
        } else if (/null/.extract(match)) {
            cls = 'null';
        }
        return cls + match;
    });
}


function extractSenteces(sentences){
    sentences[0] = sentences[0].replace("&nbsp &nbsp &nbsp", "");


    for (var a = sentences.length - 1; a >= 0; a--) {

        sentences[a] = sentences[a].trim();




        if (sentences[a].length == 2 || (sentences[a].length == 1 && a == sentences.length - 1)) {


            if (a == sentences.length - 1) {

                if (!isNaN(sentences[a][0]) || abjadya.includes(sentences[a][0])) {
                    sentences[a - 1] = sentences[a - 1] + sentences[a];

                    sentences.splice(a, 1);
                }






            }
            else {
                if (!isNaN(sentences[a][0]) || abjadya.includes(sentences[a][0])) {





                    sentences[a + 1] = sentences[a] + ' ' + sentences[a + 1];






                    sentences.splice(a, 1);


                }

            }




        }

        else {

            // detect 3alame tijarye
            if (sentences[a][sentences[a].length - 2] == 'ش' && sentences[a][sentences[a].length - 1] == '.' && sentences[a + 1][sentences[a + 1].length - 2] == 'م' && sentences[a + 2][0] == 'ل') {



                sentences[a] = sentences[a] + sentences[a + 1] + sentences[a + 2];

                sentences.splice(a + 1, 1);
                sentences.splice(a + 1, 1);

                if (a > 0) {
                    sentences[a - 1] = sentences[a - 1].trim();
                    if (!isNaN(sentences[a][0]) && (!isNaN(sentences[a - 1][sentences[a - 1].length - 2]))) {
                        sentences[a - 1] = sentences[a - 1] + sentences[a];
                        sentences.splice(a, 1);

                    }


                }


            }

            else {
                if (a != sentences.length - 1) {

                    if (!isNaN(sentences[a][0]) && a >= 1) {

                        if (!isNaN(sentences[a - 1][sentences[a - 1].length - 2])) {
                            sentences[a - 1] = sentences[a - 1] + sentences[a];
                            sentences.splice(a, 1);
                        }

                    }

                    else {
                        if (sentences[a][sentences[a].length - 1] == '.' && (sentences[a][sentences[a].length - 2] == 'د' || sentences[a][sentences[a].length - 2] == 'أ' || sentences[a][sentences[a].length - 2] == 'م')) {
                            sentences[a + 1] = sentences[a] + sentences[a + 1];
                            sentences.splice(a, 1);
                        }
                    }
                }

                else {

                    if (((!isNaN(sentences[a][0])) && sentences.length > 1)) {
                        sentences[a - 1] = sentences[a - 1] + ' ' + sentences[a];

                        sentences.splice(a, 1);


                    }
                }



            }


        }



        if (a != sentences.length - 1 && (typeof sentences[a] != 'undefined')) {


            if (sentences[a].length > 0) {
                if (sentences[a][sentences[a].length - 1] == '.'  &&  (sentences[a + 1][1] == "."  || sentences[a][[sentences[a].length - 1]] == '.' )) {
                    sentences[a + 1] = sentences[a] + ' ' + sentences[a + 1];
                    sentences.splice(a, 1);



                }
                

            }

        }


    }

    return sentences ;
}